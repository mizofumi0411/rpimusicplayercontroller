package net.mizofumi;

import java.io.IOException;

/**
 * Created by mizof on 2016/06/14.
 */
public class PlayerDisplay {

    public boolean isPlay = false;
    String title = "";
    String subtitle = "";
    int volume;
    int speed;
    String now = "";
    String max = "";

    public void setTitle(String title) {
        this.title = title.replaceAll(" ","_");
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle.replaceAll(" ","_");
    }

    public void setVolume(int volume) {
        this.volume = volume;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public void setNow(String now) {
        this.now = now;
    }

    public void setMax(String max) {
        this.max = max;
    }

    public void print(){

        System.out.println("python /home/pi/MusicPlayer/player.py play "+title+" "+subtitle+" "+volume+" "+speed+" "+now+" "+max);

        if (isPlay){
            try {
                Runtime.getRuntime().exec("python /home/pi/MusicPlayer/player.py play "+title+" "+subtitle+" "+volume+" "+speed+" "+now+" "+max);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }else {
            try {
                Runtime.getRuntime().exec("python /home/pi/MusicPlayer/player.py pause "+title+" "+subtitle+" "+volume+" "+speed+" "+now+" "+max);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}

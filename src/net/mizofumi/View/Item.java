package net.mizofumi.View;

/**
 * Created by mizof on 2016/06/13.
 */
public class Item {
    String name;
    boolean direcory;
    boolean selected;

    public Item(String name, boolean directory) {
        this.name = name;
        this.direcory = directory;
    }

    public boolean isDirecory() {
        return direcory;
    }

    public boolean isSelected() {
        return selected;
    }

    public String getName() {
        return name;
    }
}

package net.mizofumi.View;

import net.mizofumi.ListDisplay;
import java.util.List;

/**
 * Created by mizof on 2016/06/13.
 */
public class ListView {

    String title;
    public List<Item> items;
    int page = 0;
    int index = 1;
    int maxpage = 0;
    private boolean blink;

    public ListView() {
    }

    public ListView(List<Item> items) {
        this.items = items;
        countMaxPage();
    }

    public ListView(String title, List<Item> items) {
        this.title = title;
        this.items = items;
        countMaxPage();
    }

    public void setItems(List<Item> items) {
        this.items = items;
        countMaxPage();
    }

    public void countMaxPage(){
        if (items.size() % 6 == 0){
            maxpage = items.size() / 6;
        }else {
            maxpage = items.size() / 6 + 1;
        }
    }


    public void print(String title){

        System.out.println("index "+index);
        System.out.println("page "+page);

        ListDisplay listDisplay = new ListDisplay();
        listDisplay.setTitle(title);
        if (items.size() != 0){
            if ((items.size() > (page*6) && items.size() < (page*6)+6) ||  (items.size()==(page*6)+6)) {
                //リストの最後だが、数が6の倍数でない場合
                System.out.println("Type 1");
                for (int i = (page * 6); i < items.size(); i++) {
                    if (items.get(i).isDirecory()) {
                        if (index - 1 == i - (page * 6) && blink) {
                            listDisplay.addMenu(">" + items.get(i).name.replaceAll(" ", "_").replaceAll(title, "").replaceAll("/", ""));
                        } else {
                            listDisplay.addMenu("-" + items.get(i).name.replaceAll(" ", "_").replaceAll(title, "").replaceAll("/", ""));
                        }
                    } else {
                        if (index - 1 == i - (page * 6) && blink) {
                            listDisplay.addMenu(">" + items.get(i).name.replaceAll(" ", "_").replaceAll(title, "").replaceAll("/", ""));
                        } else {
                            listDisplay.addMenu("-" + items.get(i).name.replaceAll(" ", "_").replaceAll(title, "").replaceAll("/", ""));
                        }
                    }
                }
            }else {
                System.out.println("Type 2");
                for (int i = page*6; i < page*6+6; i++) {
                    if (items.get(i).isDirecory()){
                        if (index-1 == i - (page*6) && blink){
                            listDisplay.addMenu(">"+items.get(i).name.replaceAll(" ","_").replaceAll(title,"").replaceAll("/",""));
                        }else {
                            listDisplay.addMenu("-"+items.get(i).name.replaceAll(" ","_").replaceAll(title,"").replaceAll("/",""));
                        }
                    }else {
                        if (index-1 == i - (page*6)&& blink){
                            listDisplay.addMenu(">"+items.get(i).name.replaceAll(" ","_").replaceAll(title,"").replaceAll("/",""));
                        }else {
                            listDisplay.addMenu("-"+items.get(i).name.replaceAll(" ","_").replaceAll(title,"").replaceAll("/",""));
                        }
                    }
                }
            }
        }
        listDisplay.print();
    }

    public void nextItem(){
        if (index == 6){
            nextPage(1);
        }else {
            if (page+1 == maxpage){
                if (index < items.size()%6){
                    index++;
                }else if (items.size()%6 == 0){
                    index++;
                }
            }else {
                index++;
            }
        }
    }

    public void prevItem(){
        if (index == 1){
            prevPage(6);
        }else {
            index--;
        }
    }

    public void nextPage(int index){
        if (page+1<maxpage){
            this.index = index;
            page++;
        }
    }

    public void prevPage(int index){
        if (page > 0){
            this.index = index;
            page--;
        }
    }

    public int getIndex() {
        return index;
    }

    public void setBlink(boolean blink) {
        this.blink = blink;
    }

    public Item getItem() {
        try {
            return items.get((page*6)+index-1);

        }catch (Exception e){
            return null;
        }
    }
}

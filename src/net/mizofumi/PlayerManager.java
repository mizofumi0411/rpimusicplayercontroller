package net.mizofumi;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mizof on 2016/06/17.
 */
public class PlayerManager {

    int volume = 31;
    int select = 0;
    List<String> pathes;
    Player player;

    public PlayerManager() {
    }

    public PlayerManager(int select, List<String> pathes) {
        this.select = select;
        this.pathes = pathes;
    }

    public void setSelect(int select) {
        this.select = select;
    }

    public void setPathes(List<String> pathes) {
        this.pathes = pathes;
    }

    public Player getPlayer() {
        if (player == null)
            player = new Player(pathes.get(select));
        return player;
    }

}

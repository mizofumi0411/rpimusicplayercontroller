package net.mizofumi;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by mizof on 2016/06/12.
 */
public class Player {

    int speed = 0;
    String path;
    String title;
    long time;
    long maxtime;

    final String[] result = new String[1];
    Runtime runtime = Runtime.getRuntime();

    public Player(String path) {
        this.path = path;
    }

    public static boolean isPlaying(){
        String ret = "";
        boolean result = false;
        try {
            String command = "sudo /home/pi/MusicPlayer/scripts/isPlaying";
            Process process = Runtime.getRuntime().exec(command);
            BufferedReader in = new BufferedReader(new InputStreamReader(process
                    .getInputStream()));
            String line;
            while ((line = in.readLine()) != null) {
                if(!line.contains("1")){
                    result = true;
                }
            }
            in.close();
            return result;
        }catch (IOException e){
            return result;
        }
    }

    public static boolean usbPlugged(){
        String ret = "";
        boolean result = true;
        try {
            String command = "sudo /home/pi/MusicPlayer/scripts/usbPlugged";
            Process process = Runtime.getRuntime().exec(command);
            BufferedReader in = new BufferedReader(new InputStreamReader(process
                    .getInputStream()));
            String line;
            while ((line = in.readLine()) != null) {
                if(line.contains("2")){
                    result = false;
                }
            }
            in.close();
            return result;
        }catch (IOException e){
            return result;
        }
    }

    public String getSeek(){
        return execCommand("sudo /home/pi/MusicPlayer/scripts/getSeek");
    }

    public String getTitle(){
        return execCommand("sudo /home/pi/MusicPlayer/scripts/getTitle");
    }

    public String getAlbum(){
        return execCommand("sudo /home/pi/MusicPlayer/scripts/getAlbum");
    }

    public String getArtist(){
        return execCommand("sudo /home/pi/MusicPlayer/scripts/getArtist");
    }

    public void play(boolean dac){
        try {
            /*
            if (dac){
                Process process = runtime.exec("screen -c mplayer "+path+" -ao alsa:device=hw=1.0");
                System.out.println("Play "+"screen -AmdS music mplayer "+path+" -ao alsa:device=hw=1.0");
            }else {
                Process process = runtime.exec("screen -AmdS music mplayer "+path+"");
                System.out.println("Play "+"screen -AmdS music mplayer "+path+"");
            }
            */
            if (dac){
                Process process = runtime.exec("screen -AmdS music /home/pi/MusicPlayer/scripts/mplayer 1 "+path.replaceAll(" ","#")+" "+Main.playerManager.volume);
                System.out.println("Play "+"screen -AmdS music /home/pi/MusicPlayer/scripts/mplayer 1 "+path.replaceAll(" ","#")+" "+Main.playerManager.volume);
            }else {
                Process process = runtime.exec("screen -AmdS music /home/pi/MusicPlayer/scripts/mplayer 0 "+path.replaceAll(" ","#")+" "+Main.playerManager.volume);
                System.out.println("Play "+"screen -AmdS music /home/pi/MusicPlayer/scripts/mplayer 0 "+path.replaceAll(" ","#")+" "+Main.playerManager.volume);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void stop(){
        try {
            Runtime.getRuntime().exec("screen -S music -X stuff q");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void pause(){
        try {
            Runtime.getRuntime().exec("screen -S music -X stuff p");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void seekForw(){
        try {
            Runtime.getRuntime().exec("screen -S music -X stuff ^[[C");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void seekBack(){
        try {
            Runtime.getRuntime().exec("screen -S music -X stuff ^[[D");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void volumeUp(){
        if (Main.playerManager.volume < 100){
            try {
                Runtime.getRuntime().exec("screen -S music -X stuff *");
            } catch (IOException e) {
                e.printStackTrace();
            }
            Main.playerManager.volume += 3;
        }
    }

    public void volumeDown(){
        if (Main.playerManager.volume > 0){
            try {
                Runtime.getRuntime().exec("screen -S music -X stuff /");
            } catch (IOException e) {
                e.printStackTrace();
            }
            Main.playerManager.volume -= 3;
        }
    }

    public void speedUp(){
        if (speed < 14){
            try {
                Runtime.getRuntime().exec("screen -S music -X stuff ]");
            } catch (IOException e) {
                e.printStackTrace();
            }
            speed++;
        }
    }

    public void speedDown(){
        if (speed > -14){
            try {
                Runtime.getRuntime().exec("screen -S music -X stuff [");
            } catch (IOException e) {
                e.printStackTrace();
            }
            speed--;
        }
    }

    public int getSpeed() {
        return speed;
    }

    public int getVolume() {
        return Main.playerManager.volume;
    }


    public String execCommand(String command){
        String ret = "";
        try {
            Process process = Runtime.getRuntime().exec(command);
            BufferedReader in = new BufferedReader(new InputStreamReader(process
                    .getInputStream()));
            String line;
            while ((line = in.readLine()) != null) {
                ret = line;
            }
            in.close();
            return ret;
        }catch (IOException e){
            return ret;
        }
    }
}

package net.mizofumi;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mizof on 2016/06/13.
 */
public class ListDisplay {

    public String title = "";
    public String menu1 = "";
    public String menu2 = "";
    public String menu3 = "";
    public String menu4 = "";
    public String menu5 = "";
    public String menu6 = "";

    public List<String> menues = new ArrayList<>();

    public void setTitle(String title) {
        this.title = title.replaceAll(" ","　");
    }

    public void addMenu(String menu){
        menues.add(menu);
    }

    public void print(){

        if (menues.size() == 0){
            try {
                Runtime.getRuntime().exec("python /home/pi/MusicPlayer/drawtext.py 0,0,"+ title +" 0,6,"+ menu1 +" 0,13,"+ menu2 +" 0,20,"+ menu3 +" 0,27,"+ menu4 +" 0,34,"+ menu5 +" 0,41,"+menu6);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }else {

            if (menues.size() < 6){
                for (int i = 0; i < 6; i++) {
                    addMenu("");
                }
            }

            try {
                Runtime.getRuntime().exec("python /home/pi/MusicPlayer/drawtext.py 0,0,"+ title +" 0,6,"+ menues.get(0) +" 0,13,"+ menues.get(1) +" 0,20,"+ menues.get(2) +" 0,27,"+ menues.get(3) +" 0,34,"+ menues.get(4) +" 0,41,"+menues.get(5));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void clear(){
        try {
            Runtime.getRuntime().exec("python /home/pi/MusicPlayer/clear.py");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void printList(List<String> list){

        String args = "";
        for (int i = 1; i < list.size()+1; i++) {
            args += "0,"+((i*6))+","+list.get(i-1)+" ";
        }

        try {
            System.out.println("python /home/pi/MusicPlayer/drawtext.py 0,0, "+args);
            Runtime.getRuntime().exec("python /home/pi/MusicPlayer/drawtext.py 0,0, "+args);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

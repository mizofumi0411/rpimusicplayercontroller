package net.mizofumi.DAC;

/**
 * Created by mizof on 2016/06/23.
 */
public interface DACListener {

    void plugedDAC();
    void unplugedDAC();

}

package net.mizofumi;

/**
 * Created by mizof on 2016/06/12.
 */
public interface ButtonListener {

    void onRedClicked();
    boolean onRedLongClicked();

    void onYellowClicked();
    boolean onYellowLongClicked();

    void onGreenClicked();
    boolean onGreenLongClicked();

    void onWhiteClicked();
    boolean onWhiteLongClicked();

    void onBlueClicked();
    boolean onBlueLongClicked();

    boolean onRightWhiteLongClicked();

    void onRightWhiteClicked();

    boolean onLeftWhiteLongClicked();

    void onLeftWhiteClicked();
}

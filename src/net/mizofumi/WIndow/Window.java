package net.mizofumi.WIndow;

import net.mizofumi.ButtonListener;
import net.mizofumi.ButtonWatcher;
import net.mizofumi.DAC.DACListener;
import net.mizofumi.Player;

import java.util.Random;

/**
 * Created by mizof on 2016/06/14.
 */
public class Window implements ButtonListener,DACListener {

    Window backWindow;
    boolean rendering = true;
    private final ButtonWatcher watcher;
    protected int windowId = new Random().nextInt(5000);
    protected boolean isShow = true;
    int changeDelay = 100;
    int renderingDelay = 250;
    boolean isDacPluged = false;

    public Window() {
        watcher = new ButtonWatcher(this);
        watcher.watch();
    }

    public void setBackWindow(Window backWindow) {
        this.backWindow = backWindow;
        this.backWindow.rendering = false;
    }

    public void render(){

    }

    public void kill(){
        rendering = false;
        isShow = false;
        watcher.stop();
    }

    public void run(){
        try {
            Thread.sleep(WindowSetting.changeDelay);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        while (isShow){
            if (Player.usbPlugged()){
                if (!isDacPluged){
                    isDacPluged = true;
                    plugedDAC();
                }
            }else {
                if (isDacPluged){
                    isDacPluged = false;
                    unplugedDAC();
                }
            }

            render();


            try {
                Thread.sleep(renderingDelay);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void jumpHome(){
        kill();
    }

    public void jumpBack(){
        kill();
        if (backWindow != null)
            backWindow.notifyFinishForeground();
    }

    public int getWindowId() {
        return windowId;
    }

    public void notifyFinishForeground(){
        rendering = true;
    }

    @Override
    public void onRedClicked() {

    }

    @Override
    public boolean onRedLongClicked() {
        return false;
    }

    @Override
    public void onYellowClicked() {

    }

    @Override
    public boolean onYellowLongClicked() {
        return false;
    }

    @Override
    public void onGreenClicked() {

    }

    @Override
    public boolean onGreenLongClicked() {
        return false;
    }

    @Override
    public void onWhiteClicked() {

    }

    @Override
    public boolean onWhiteLongClicked() {
        return false;
    }

    @Override
    public void onBlueClicked() {

    }

    @Override
    public boolean onBlueLongClicked() {
        return false;
    }

    @Override
    public boolean onRightWhiteLongClicked() {
        return false;
    }

    @Override
    public void onRightWhiteClicked() {

    }

    @Override
    public boolean onLeftWhiteLongClicked() {
        return false;
    }

    @Override
    public void onLeftWhiteClicked() {

    }

    @Override
    public void plugedDAC() {

    }

    @Override
    public void unplugedDAC() {

    }
}

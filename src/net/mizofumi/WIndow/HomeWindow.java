package net.mizofumi.WIndow;

import net.mizofumi.ListDisplay;
import net.mizofumi.Player;

import java.io.IOException;

/**
 * Created by mizof on 2016/06/14.
 */
public class HomeWindow extends Window {

    boolean redLong = false;
    boolean greenLong = false;
    private int select = 0;
    private boolean a = false;
    String title = "　　　Mainmenu";

    @Override
    public void render() {
        super.render();
        if (!rendering){
            return;
        }

        System.out.println("Playing "+Player.isPlaying());

        System.out.println("Menu"+windowId);
        ListDisplay listDisplay = new ListDisplay();
        listDisplay.title = title;
        listDisplay.menu1 = "-ファイラ";
        listDisplay.menu2 = "-設定";
        listDisplay.menu3 = "-本体情報";
        listDisplay.menu4 = "-ツール";
        if (Player.isPlaying())
            listDisplay.menu5 = "-再生中";

        if (select == 0){
            if (a){
                listDisplay.menu1 = ">ファイラ";
            }
        }
        if (select == 1){
            if (a){
                listDisplay.menu2 = ">設定";
            }
        }
        if (select == 2){
            if (a){
                listDisplay.menu3 = ">本体情報";
            }
        }
        if (select == 3){
            if (a){
                listDisplay.menu4 = ">ツール";
            }
        }
        if (select == 4 && Player.isPlaying()){
            if (a){
                listDisplay.menu5 = ">再生中";
            }
        }
        a = !a;
        listDisplay.print();
    }

    @Override
    public void plugedDAC() {
        super.plugedDAC();
        title = "　　Mainmenu[DAC]";
        System.out.println("DAC Plugged");
    }

    @Override
    public void unplugedDAC() {
        super.unplugedDAC();
        title = "　　　Mainmenu";
        System.out.println("DAC UnPlugged");
    }

    @Override
    public void onBlueClicked() {
        select -= 1;
        super.onBlueClicked();
    }

    @Override
    public void onYellowClicked() {
        select += 1;
        super.onYellowClicked();
    }

    @Override
    public void onWhiteClicked() {
        super.onWhiteClicked();
        if (select == 0){
            rendering = false;
            FilerWindow filerWindow = new FilerWindow();
            filerWindow.setBackWindow(this);
            filerWindow.run();
        }

        if (select == 2){
            rendering = false;
            StatusWindow statusWindow = new StatusWindow();
            statusWindow.setBackWindow(this);
            statusWindow.run();
        }

        if (select == 3){
            rendering = false;
            ToolWindow toolWindow = new ToolWindow();
            toolWindow.setBackWindow(this);
            toolWindow.run();
        }
    }

    @Override
    public boolean onRedLongClicked() {
        redLong = true;
        if (greenLong){
            new ListDisplay().clear();
            kill();
        }
        return super.onRedLongClicked();
    }

    @Override
    public boolean onGreenLongClicked() {
        greenLong = true;
        if (redLong){
            new ListDisplay().clear();
            kill();
        }
        return super.onGreenLongClicked();
    }

    @Override
    public void notifyFinishForeground() {
        super.notifyFinishForeground();
    }


}

package net.mizofumi.WIndow;

import net.mizofumi.ListDisplay;

import java.io.IOException;

/**
 * Created by mizofumi0411 on 2016/06/15.
 */
public class ToolWindow extends Window {

    private int select = 0;
    private boolean a = false;

    @Override
    public void render() {
        super.render();

        ListDisplay listDisplay = new ListDisplay();
        listDisplay.title = "　　　　Tool";
        listDisplay.menu1 = "-天気予報";
        listDisplay.menu2 = "-交通情報";
        listDisplay.menu3 = "-電源OFF";
        listDisplay.menu4 = "-再起動";

        if (select == 0){
            if (a){
                listDisplay.menu1 = ">天気予報";
            }
        }
        if (select == 1){
            if (a){
                listDisplay.menu2 = ">交通情報";
            }
        }
        if (select == 2){
            if (a){
                listDisplay.menu3 = ">電源OFF";
            }
        }
        if (select == 3){
            if (a){
                listDisplay.menu4 = ">再起動";
            }
        }

        a = !a;
        listDisplay.print();
    }

    @Override
    public void onLeftWhiteClicked() {
        jumpBack();
    }

    @Override
    public void onBlueClicked() {
        select -= 1;
        super.onBlueClicked();
    }

    @Override
    public void onYellowClicked() {
        select += 1;
        super.onYellowClicked();
    }

    @Override
    public void onWhiteClicked() {
        super.onWhiteClicked();
        if (select == 2){
            try {
                Runtime.getRuntime().exec("sudo poweroff");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (select == 3){
            try {
                Runtime.getRuntime().exec("sudo reboot");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onRightWhiteClicked() {
        jumpHome();
    }

}

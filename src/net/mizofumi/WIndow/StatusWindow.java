package net.mizofumi.WIndow;

import net.mizofumi.ListDisplay;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;

/**
 * Created by mizof on 2016/06/14.
 */
public class StatusWindow extends Window {

    private final String lanip;
    private final String wlanip;

    public StatusWindow() {
        lanip = getIP("eth0");
        wlanip = getIP("wlan0");
    }

    @Override
    public void render() {
        super.render();
        System.out.println("Status"+windowId);


        ListDisplay listDisplay = new ListDisplay();
        listDisplay.title = "　　　　Status";
        listDisplay.menu1 = "LANIP:"+lanip;
        listDisplay.menu2 = "WLANIP:"+wlanip;
        listDisplay.print();

    }

    @Override
    public void onLeftWhiteClicked() {
        jumpBack();
    }

    @Override
    public boolean onGreenLongClicked() {
        /*
        kill();
        try {
            Thread.sleep(WindowSetting.changeDelay);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        new HomeWindow().run();
        */
        return super.onGreenLongClicked();
    }

    @Override
    public void onRightWhiteClicked() {
        jumpHome();
    }

    protected String getIP(String type){
        String ret = "";

        Enumeration<NetworkInterface> eni = null;
        try {
            eni = NetworkInterface.getNetworkInterfaces();

            while (eni.hasMoreElements()) {
                NetworkInterface ni = eni.nextElement();
                Enumeration<InetAddress> inetAddresses =  ni.getInetAddresses();


                while(inetAddresses.hasMoreElements()) {
                    InetAddress ia = inetAddresses.nextElement();
                    if(!ia.isLinkLocalAddress()) {

                        if (ni.getName().equals(type)){
                            ret = ia.getHostAddress();
                        }

                    }
                }
            }
        } catch (SocketException e) {
            e.printStackTrace();
        }
        return ret;
    }
}

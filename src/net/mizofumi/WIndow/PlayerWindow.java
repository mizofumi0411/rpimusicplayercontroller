package net.mizofumi.WIndow;

import net.mizofumi.Player;
import net.mizofumi.PlayerDisplay;

/**
 * Created by mizof on 2016/06/14.
 */
public class PlayerWindow extends Window {

    PlayerDisplay display;
    Player player;
    String path;
    String title;

    public PlayerWindow(String path, String title) {
        this.path = path;
        this.title = title;
        player = new Player(path);
        display = new PlayerDisplay();
        if (player.isPlaying())
            player.stop();
        player.play(player.usbPlugged());
        display.isPlay = true;

    }

    @Override
    public void render() {
        super.render();
        if (!rendering){
            return;
        }

        display.setTitle(title);
        if (player.getArtist().length() != 0){
            display.setSubtitle(player.getArtist());
        }else {
            display.setSubtitle("unknown");
        }
        display.setVolume(player.getVolume());
        display.setSpeed(player.getSpeed());
        display.print();

        String[] seeks = player.getSeek().split(",");
        if (seeks.length > 0){
            if (seeks[0].length() == 4){
                System.out.println("NOW 00:"+seeks[0]);
                display.setNow("00:"+seeks[0]);
            }else {
                System.out.println("NOW "+seeks[0]);
                display.setNow(seeks[0]);
            }
            display.setMax(seeks[1]);
            System.out.println("MAX "+seeks[1]);
        }

        if (!player.isPlaying()){
            kill();
            try {
                Thread.sleep(changeDelay);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            new FilerWindow().run();
        }
    }

    @Override
    public void unplugedDAC() {
        super.unplugedDAC();
        player.stop();
    }


    @Override
    public void onWhiteClicked() {
        super.onWhiteClicked();
        //python player.py pause 凸凹 80 40
        display.isPlay = !display.isPlay;
        player.pause();
    }

    @Override
    public void onRedClicked() {
        super.onRedClicked();
        player.seekForw();
    }

    @Override
    public void onGreenClicked() {
        super.onGreenClicked();
        player.seekBack();
    }

    @Override
    public boolean onRedLongClicked() {
        player.seekForw();
        return false;
    }

    @Override
    public boolean onGreenLongClicked() {
        player.seekBack();
        return false;
    }


    @Override
    public void onBlueClicked() {
        super.onBlueClicked();
        player.speedUp();
    }

    @Override
    public void onYellowClicked() {
        super.onYellowClicked();
        player.speedDown();
    }

    @Override
    public boolean onWhiteLongClicked() {
        player.stop();
        return false;
    }

    @Override
    public boolean onBlueLongClicked() {
        player.volumeUp();
        return false;
    }

    @Override
    public boolean onYellowLongClicked() {
        player.volumeDown();
        return false;
    }

    @Override
    public void onRightWhiteClicked() {
        jumpHome();
    }

    @Override
    public void onLeftWhiteClicked() {
        jumpBack();
    }
}

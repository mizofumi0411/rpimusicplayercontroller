package net.mizofumi.WIndow;

import net.mizofumi.View.Item;
import net.mizofumi.View.ListView;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by mizof on 2016/06/14.
 */
public class FilerWindow extends Window {

    boolean blink = false;
    protected int windowId = new Random().nextInt(5000);
    String path = "/media/usb";
    File[] files;
    private ListView listView;

    public FilerWindow() {
        listView = new ListView();
        openDelay();
    }

    public FilerWindow(String path){
        listView = new ListView();
        this.path = path;
        openDelay();
    }

    public File[] getFiles(String path){
        this.path = path;
        File dir = new File(path);
        files = dir.listFiles();
        return files;
    }

    protected void openDelay(){
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void render() {
        super.render();
        if (!rendering){
            return;
        }
        System.out.println("Filer"+windowId);

        List<Item> items = new ArrayList<>();
        for (File file : getFiles(path)){
            items.add(new Item(file.getPath(),file.isDirectory()));
        }

        listView.setItems(items);
        listView.setBlink(blink);
        listView.print(path);
        blink = !blink;

        try {
            Thread.sleep(renderingDelay+100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public String getBackPath(String path){
        if (path.equals("/media/usb"))
            return path;
        String[] nowdirs = path.split("/");
        String result = "";
        for (int i = 1; i < nowdirs.length-1; i++) {
            result += "/"+nowdirs[i];
        }
        if(result.equals("")) result = "/";
        return result;
    }

    @Override
    public boolean onWhiteLongClicked() {
        kill();
        try {
            Thread.sleep(WindowSetting.changeDelay);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        new FilerWindow(getBackPath(path)).run();

        return false;
    }

    @Override
    public void onWhiteClicked() {
        super.onWhiteClicked();
        Item item = listView.getItem();
        if (item == null)
            return;
        if (item.isDirecory()){
            System.out.println("Dir "+item.getName());
            FilerWindow filerWindow = new FilerWindow(item.getName());
            filerWindow.setBackWindow(this);
            filerWindow.run();
        }else {

            System.out.println("File "+item.getName());
            PlayerWindow playerWindow = new PlayerWindow(item.getName(),item.getName().replaceAll(path,""));
            playerWindow.setBackWindow(this);
            playerWindow.run();

        }
    }

    @Override
    public void onLeftWhiteClicked() {
        super.onLeftWhiteClicked();
        jumpBack();
    }

    @Override
    public void onRedClicked() {
        super.onRedClicked();
        listView.nextPage(1);
    }

    @Override
    public void onGreenClicked() {
        super.onGreenClicked();
        listView.prevPage(1);
    }

    @Override
    public void onBlueClicked() {
        super.onBlueClicked();
        listView.prevItem();
    }

    @Override
    public void onYellowClicked() {
        super.onYellowClicked();
        listView.nextItem();
    }

    @Override
    public boolean onBlueLongClicked() {
        listView.prevItem();
        return super.onBlueLongClicked();
    }

    @Override
    public boolean onYellowLongClicked() {
        listView.nextItem();
        return super.onYellowLongClicked();
    }

    @Override
    public void onRightWhiteClicked() {
        jumpHome();
    }

}

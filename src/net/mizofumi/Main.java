package net.mizofumi;

import net.mizofumi.WIndow.HomeWindow;

import java.io.*;

public class Main {

    public static PlayerManager playerManager = new PlayerManager();

    public static void main(String[] args) throws IOException, InterruptedException {

        Runtime.getRuntime().exec("python /home/pi/MusicPlayer/start.py");

        Thread.sleep(2000);

        new HomeWindow().run();

    }
}

package net.mizofumi;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.*;

/**
 * Created by mizof on 2016/06/12.
 */
public class ButtonWatcher {

    ButtonListener listener;
    Button red;
    Button yellow;
    Button green;
    Button white;
    Button blue;
    Button rwhite;
    Button lwhite;

    private boolean stop = false;

    boolean redlongAfterClickable = true;
    boolean yellowlongAfterClickable = true;
    boolean greenlongAfterClickable = true;
    boolean whitelongAfterClickable = true;
    boolean bluelongAfterClickable = true;
    boolean rwhitelongAfterClickable = true;
    boolean lwhitelongAfterClickable = true;

    protected int longClickJudgeCount = 20;

    public ButtonWatcher(ButtonListener listener) {
        this.listener = listener;
        red = new Button("/sys/class/gpio/gpio20/value");
        yellow = new Button("/sys/class/gpio/gpio21/value");
        green = new Button("/sys/class/gpio/gpio13/value");
        white = new Button("/sys/class/gpio/gpio19/value");
        blue = new Button("/sys/class/gpio/gpio26/value");
        rwhite = new Button("/sys/class/gpio/gpio12/value");
        lwhite = new Button("/sys/class/gpio/gpio16/value");
    }

    public void stop(){
        stop = true;
    }

    public void watch()  {

        new Thread(new Runnable() {
            @Override
            public void run() {

                while(!stop){
                    if (cat(red.path)){
                        red.count++;
                        if (red.count>=longClickJudgeCount){
                            redlongAfterClickable = listener.onRedLongClicked();
                            red.count = 0;
                        }
                    }else if (red.count != 0){
                        if (redlongAfterClickable)
                            listener.onRedClicked();
                        red.count = 0;
                    }else {
                        redlongAfterClickable = true;
                    }

                    if (cat(yellow.path)){
                        yellow.count++;
                        if (yellow.count>=longClickJudgeCount){
                            yellowlongAfterClickable = listener.onYellowLongClicked();
                            yellow.count = 0;
                        }
                    }else if (yellow.count != 0){
                        if (yellowlongAfterClickable)
                            listener.onYellowClicked();
                        yellow.count = 0;
                    }else {
                        yellowlongAfterClickable = true;
                    }

                    if (cat(green.path)){
                        green.count++;
                        if (green.count>=longClickJudgeCount){
                            greenlongAfterClickable = listener.onGreenLongClicked();
                            green.count = 0;
                        }
                    }else if (green.count != 0){
                        if (greenlongAfterClickable)
                            listener.onGreenClicked();
                        green.count = 0;
                    }else {
                        greenlongAfterClickable = true;
                    }

                    if (cat(white.path)){
                        white.count++;
                        if (white.count>=longClickJudgeCount){
                            whitelongAfterClickable = listener.onWhiteLongClicked();
                            white.count = 0;
                        }
                    }else if (white.count != 0){
                        if (whitelongAfterClickable)
                            listener.onWhiteClicked();
                        white.count = 0;
                    }else {
                        whitelongAfterClickable = true;
                    }

                    if (cat(blue.path)){
                        blue.count++;
                        if (blue.count>=longClickJudgeCount){
                            bluelongAfterClickable = listener.onBlueLongClicked();
                            blue.count = 0;
                        }
                    }else if (blue.count != 0){
                        if (bluelongAfterClickable)
                            listener.onBlueClicked();
                        blue.count = 0;
                    }else {
                        bluelongAfterClickable = true;
                    }

                    if (cat(rwhite.path)){
                        rwhite.count++;
                        if (rwhite.count>=longClickJudgeCount){
                            rwhitelongAfterClickable = listener.onRightWhiteLongClicked();
                            rwhite.count = 0;
                        }
                    }else if (rwhite.count != 0){
                        if (rwhitelongAfterClickable)
                            listener.onRightWhiteClicked();
                        rwhite.count = 0;
                    }else {
                        rwhitelongAfterClickable = true;
                    }

                    if (cat(lwhite.path)){
                        lwhite.count++;
                        if (lwhite.count>=longClickJudgeCount){
                            lwhitelongAfterClickable = listener.onLeftWhiteLongClicked();
                            lwhite.count = 0;
                        }
                    }else if (lwhite.count != 0){
                        if (lwhitelongAfterClickable)
                            listener.onLeftWhiteClicked();
                        lwhite.count = 0;
                    }else {
                        lwhitelongAfterClickable = true;
                    }

                }
            }
        }).start();
    }

    protected boolean cat(String path){

        String ret = "";
        boolean result = false;
        try {
            String command = "cat "+path;
            Process process = Runtime.getRuntime().exec(command);
            BufferedReader in = new BufferedReader(new InputStreamReader(process
                    .getInputStream()));
            String line;
            while ((line = in.readLine()) != null) {
                if(line.contains("1")){
                    result = true;
                }
            }
            in.close();
            return result;
        }catch (IOException e){
            return result;
        }
    }

    class Button{
        String path;
        int count;

        public Button(String path) {
            this.path = path;
        }
    }
}
